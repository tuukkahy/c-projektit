/* Ohjelma, joka auttaa maankäytön suunnittelussa. */
/* Tuukka Hyvärinen */
/* maansuunnittelu.c */

/**---------------------------------------------------------------------**/
/*
Ongelmat:

    -Kasveja poistettaessa kartasta ne voivat jäädä vielä listalle
        *Korjaa lisäämällä poista-funktion loppuun rutiini, joka tarkastaa onko kaikkia listan kasveja vielä kartalla.
         Jos jokin kasvi puuttuu kartasta, siirretään se listan loppuun ja sitten vähennetään yksi kasvienlkm:stä.
         Tehdään näin kaikille puuttuville kasveille.

    -Tallentaessa tiedostoon viivat ja kulmat eivät tulostu kunnolla
        *Selvitä miten tulostaa viivoja ja kulmia ja ota se käyttöön tulostaTiedostoon-funktiossa.

    -Rakentaminen ja päällekkäisyys hieman epäselvää vielä.

    -Kasvien listalla varataan turhaan liikaa muistia
        *Käytä heap-varausta, jossa varataan kasvilistalle tilaa vain tarvittava määrä ja lisätään sekä vapautetaan
         muistia tarpeen mukaan.

    -Ohjelma on hankalakäyttöinen koska käyttäjän pitää laskea mihin kohtaa rakennukset ja kasvit tulevat
        *Graafinen käyttöliittymä voi ratkaista ongelman tai sitten jonkinlainen ristikko missä näkyy kunkin 1x1 alueen arvo, jolloin
         käyttäjä voi helposti nähdä ja päättää mihin uusi objekti laitetaan.


*/
/**---------------------------------------------------------------------**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE * fptr;

// Kasvin tiedot sisältävä tietue.
struct kasvi {
    char nimi[25];
    char merkki;
    int kasteluvali;
    int kylvoaika;
    int satoaika;
    int ok; //Muuttuja joka kertoo onko kasvin tiedot annettu oikein
};

void tulostaTervehdys();
int haePituus();
int haeLeveys();
void rakennaALue(int pituus, int leveys, char alue[][leveys]);
void tulostaAlue(int pituus, int leveys, char alue[][leveys]);
void lisaaRakennus(int pituus, int leveys, char alue[][leveys]);
struct kasvi lisaaKasvi(int pituus, int leveys, char alue[][leveys], struct kasvi kasvit[], int kasvienlkm);;
void tulostaTiedot(struct kasvi kasvit[], int kasvienlkm);
void tulostaKaikki(struct kasvi kasvit[], int kasvienlkm);
void poista(int pituus, int leveys, char alue[][leveys]);
void tallennaTiedostoon(int pituus, int leveys, char alue[][leveys], struct kasvi kasvit[], int kasvienlkm);

main()
{
    int pituus, leveys;
    char valinta;
    int kasvienlkm = 0;
    struct kasvi kasvit[50];
    struct kasvi testiKasvi;
    int i;
    int onjo;
    char tallennusvalinta;


    // Tulostetaan tervehdys
    tulostaTervehdys();

    // Pyydetään käyttäjältä alueen pituus ja leveys varmistaen, että se pysyy tietyissä mitoissa.

    pituus = haePituus();
    leveys = haeLeveys();

    // Luodaan alue käyttäen rakennaAlue-funktiota
    char alue[pituus][leveys];
    rakennaAlue(pituus, leveys, alue);


    // Muodostetaan silmukka, jossa tehdään mitä käyttäjä haluaa kunnes käyttäjä päättää valita lopettamisen
    do
    {
        tulostaAlue(pituus, leveys, alue);

        printf("Mit\x84 haluat tehd\x84 seuraavaksi (Sy\x94t\x84 jokin seuraavista merkeist\x84)?\n\n");
        printf("Lis\x84\x84 rakennus\t\t\t = \t R\n");
        printf("Lis\x84\x84 kasvi\t\t\t = \t K\n");
        printf("Poista valitun alueen sis\x84lt\x94\t = \t P\n");
        printf("Tulosta kasvin tiedot\t\t = \t T\n");
        printf("Tulosta kaikkien kasvien tiedot\t = \t A\n");
        printf("Lopeta\t\t\t\t = \t L\n\n");


        printf("Sy\x94t\x84 valintasi: ");
        scanf(" %c", &valinta);
        valinta = toupper(valinta);

        switch (valinta)
        {
            case ('R'):
            {
                lisaaRakennus(pituus, leveys, alue);
                break;
            }

            case ('K'):
            {
                testiKasvi = lisaaKasvi(pituus, leveys, alue, kasvit, kasvienlkm);
                // Varmistetaan, että kasvin tiedot on annettu oikein.
                if (testiKasvi.ok == 1)
                {
                    // Testataan onko annettu kasvi jos olemassa, eikä lisätä sitä kasvitaulukkoon jos näin on
                    onjo = 0;
                    for (i = 0; i < 50; i++)
                    {
                        if (testiKasvi.merkki == kasvit[i].merkki)
                        {
                            onjo = 1;
                        }
                    }
                    if (onjo == 0)
                    {
                        kasvit[kasvienlkm] = testiKasvi;
                        kasvienlkm += 1;
                    }
                }
                break;
            }

            case ('T'):
            {
                tulostaTiedot(kasvit, kasvienlkm);
                break;
            }

            case ('A'):
            {
                tulostaKaikki(kasvit, kasvienlkm);
                break;
            }
            case ('P'):
            {
                poista(pituus, leveys, alue);
                break;
            }

            case ('L'):
            {
                printf("Haluatko tallentaa luodun alueen ja kasvit tiedostoon? (K/E)");
                scanf(" %c", &tallennusvalinta);
                tallennusvalinta = toupper(tallennusvalinta);
                if (tallennusvalinta == 'K')
                {
                    tallennaTiedostoon(pituus, leveys, alue, kasvit, kasvienlkm);
                }
                printf("Lopetetaan...");
                break;
            }
            default:
            {
                printf("Kelpaamaton valinta! Yrit\x84 uudelleen");
                break;
            }
        }
    }
    while (valinta != 'L');

    return(0);
}


/******************************************************************************************************/


void tulostaTervehdys()
{
    printf("Tervetuloa suunnittelemaan maank\x84ytt\x94\x84!");
    printf("\n*****************************************\n");
    printf("T\x84m\x84n ohjelman avulla pystyt suunnittelemaan esim. m\x94kkitontin k\x84ytt\x94\x84.\n\n");
}


/******************************************************************************************************/


int haePituus()
{
    int pituus;

    do
    {
        printf("Sy\x94t\x84 alueen pituus metrein\x84\n");
        printf("Anna arvo v\x84lilt\x84 1-50: ");
        scanf(" %d", &pituus);

        if (pituus < 1 || pituus > 50)
        {
        printf("Virheellinen pituus!");
        printf("Yrit\x84 uudestaan.\n");
        }

    }
    while (pituus < 1 || pituus > 50);

    // Lisätään pituuteen palautettaessa kaksi reunaviivoja varten.
    return pituus +2;
}


/******************************************************************************************************/


int haeLeveys()
{
    int leveys;

    do
    {
        printf("Sy\x94t\x84 alueen leveys metrein\x84\n");
        printf("Anna arvo v\x84lilt\x84 1-50: ");
        scanf(" %d", &leveys);

        if (leveys < 1 || leveys > 50)
        {
            printf("Virheellinen leveys!");
            printf("Yrit\x84 uudestaan.\n");
        }

    }
    while (leveys < 1 || leveys > 50);
    // Lisätään pituuteen palautettaessa kaksi reunaviivoja varten.
    return leveys +2;
}


/******************************************************************************************************/


void rakennaAlue(int pituus, int leveys, char alue[][leveys])
{
    int i, j;

    // Laitetaan ensin kulmat
    alue[0][0] = '\xDA';
    alue[0][leveys-1] = '\xBF';
    alue[pituus-1][0] = '\xC0';
    alue[pituus-1][leveys-1] = '\xD9';

    // Sitten lisätään loput reunaviivat
    for (i = 1; i < pituus-1; i++)
    {
        alue[i][0] = '\xB3';
        alue[i][leveys-1] = '\xB3';
    }

    for (i = 1; i < leveys-1; i++)
    {
        alue[0][i] = '\xC4';
        alue[pituus-1][i] = '\xC4';
    }

    // Lopuksi täytetään sisäpuoli koknaan merkillä t (=tyhjä), joka esitetään tulostaessa välilyännillä.
    for (i = 1; i < pituus-1; i++)
    {
        for (j = 1; j < leveys-1; j++)
        {
            alue[i][j] = 't';
        }
    }
}


/******************************************************************************************************/


void tulostaAlue(int pituus, int leveys, char alue[][leveys])
{
    printf("\n\nT\x84lt\x84 alue n\x84ytt\x84\x84 t\x84ll\x84 hetkell\x84:\n");

    for (int i = 0; i < pituus; i++)
    {
        for (int j = 0; j < leveys; j++)
        {
            // Jos taulukon arvo on t (=tyhjä) tai r (=rakennus), tulostetaan välilyänti
            if (alue[i][j] == 't' || alue[i][j] == 'r')
                printf(" ");

            // Muussa tapauksessa tulostetaan taulukon merkki.
            else
                printf("%c", alue[i][j]);
        }

        printf("\n");
    }
}


/******************************************************************************************************/


void lisaaRakennus(int pituus, int leveys, char alue[][leveys])
{
    int alkurivi, alkusarake;
    int rakennuspituus, rakennusleveys;
    int i, j;
    int sopii;

    // Otetaan käyttäjältä rakennuksen paikka ja mitat varmistaen, että tuleva rakennus pysyy annetulla alueella.


    do
    {
        sopii = 1;

        // Pyydetään ensin käyttäjältä rakennuksen lähtäkulman paikka eli luoteiskulma.
        printf("Miss\x84 on rakennuksen luoteiskulma?");
        printf("\nAnna et\x84isyys pohjoissreunasta: ");
        scanf(" %d", &alkurivi);
        printf("Anna et\x84isyys l\x84nsisreunasta: ");
        scanf(" %d", &alkusarake);


        // Seuraavaksi pyydetään rakennuksen pituus ja leveys.
        printf("\nMik\x84 on rakennuksen pituus metrein\x84: ");
        scanf(" %d", &rakennuspituus);
        printf("Mik\x84 on rakennuksen leveys metrein\x84: ");
        scanf(" %d", &rakennusleveys);

        if (alkurivi + rakennuspituus >= pituus)
        {
            sopii = 0;
            printf ("Virhe! Annettu rakennus ei sovi alueellle! Sy\x94t\x84 arvot uudestaan:\n");
            break;
        }
        if (alkusarake + rakennusleveys >= leveys)
        {
            sopii = 0;
            printf ("Virhe! Annettu rakennus ei sovi alueellle! Sy\x94t\x84 arvot uudestaan:\n");
            break;
        }

    }
    while (sopii == 0);

    // Varmistetaan, että rakennus ei tule minkään jo olemassa olevan päälle
    for (i = alkurivi; i < (alkurivi+rakennuspituus); i++)
    {
        for (j = alkusarake; j < (alkusarake+rakennusleveys); j++)
        {
            if (alue[i][j] != 't')
            {
                printf("Virhe! Annetulla alueella on jo joko kasveja tai rakennuksia!");
                return;
            }
        }
    }

    // Lisätään lopuksi rakennus taulukkoon.

    // Laitetaan ensin kulmat
    alue[alkurivi][alkusarake] = '\xDA';
    alue[alkurivi][alkusarake+rakennusleveys-1] = '\xBF';
    alue[alkurivi+rakennuspituus-1][alkusarake] = '\xC0';
    alue[alkurivi+rakennuspituus-1][alkusarake+rakennusleveys-1] = '\xD9';

    // Sitten lisätään loput reunaviivat
    for (i = (alkurivi+1); i < (alkurivi+rakennuspituus-1); i++)
    {
        alue[i][alkusarake] = '\xB3';
        alue[i][alkusarake+rakennusleveys-1] = '\xB3';
    }

    for (i = (alkusarake+1) ; i < (alkusarake+rakennusleveys-1); i++)
    {
        alue[alkurivi][i] = '\xC4';
        alue[alkurivi+rakennuspituus-1][i] = '\xC4';
    }

    // Lopuksi täytetään rakennuksen sisältä r-merkeillä, jotta rakennuksen sisään ei pystytä lisäämään esim kasveja.
    for (i = (alkurivi+1); i < (alkurivi+rakennuspituus-1); i++)
    {
        for (j = (alkusarake+1); j < (alkusarake+rakennusleveys-1); j++)
        {
            alue[i][j] = 'r';
        }
    }
}


/******************************************************************************************************/


struct kasvi lisaaKasvi(int pituus, int leveys, char alue[][leveys], struct kasvi kasvit[], int kasvienlkm)
{
    int alkurivi, alkusarake;
    int kasvipituus, kasvileveys;
    int i, j;
    int sopii;
    int onjo, merkki;

    // Luodaan tietue kasville, johon käyttäjä voi tallentaa tietoa kasvista.
    struct kasvi annettuKasvi;

    // Pyydetään ensin käyttäjältä kasvin tiedot
    printf("Anna kasvin nimi: ");
    getchar();
    gets(annettuKasvi.nimi);

    // Tarkistetaan onko kasvi annettu jo kerran ja jos näin on, ei pyydetä loppuja tietoja uudestaan.
    onjo = 0;
    for (i = 0; i < kasvienlkm; i++)
    {
        if (strcmp(kasvit[i].nimi, annettuKasvi.nimi) == 0)
        {
            onjo = 1;
            merkki = i;
        }
    }
    if (onjo == 1)
    {
        annettuKasvi = kasvit[merkki];
    }
    else
    {
        // Varmistetaan, että käyttäjä ei anna samaa merkkiä useammalle kasville.
        do
        {
            sopii = 1;
            printf("Anna kasvin symboli karttaa varten (\x84l\x84 k\x84yt\x84 merkkej\x84 r tai t): ");
            scanf(" %c", &annettuKasvi.merkki);

            for (i = 0; i < kasvienlkm; i++)
            {
                if (annettuKasvi.merkki == kasvit[i].merkki)
                {
                    printf("Virhe! Annettu merkki on jo k\x84yt\x94ss\x84. Yrit\x84 uudestaan.\n");
                    sopii = 0;
                }
            }
        }
        while (sopii == 0);
        printf("Anna kasvin kasteluv\x84li: ");
        scanf(" %d", &annettuKasvi.kasteluvali);
        do
        {
            printf("Anna kasvin kylv%caika (kuukauden numero): ", '\x94');
            scanf(" %d", &annettuKasvi.kylvoaika);

            if (annettuKasvi.kylvoaika < 1 ||annettuKasvi.kylvoaika > 12)
                printf("Virheellinen kuukausi! Yrit\x84 uudestaan:\n");

        }
        while (annettuKasvi.kylvoaika < 1 ||annettuKasvi.kylvoaika > 12);
        do
        {
            printf("Anna kasvin satoaika (kuukauden numero): ");
            scanf(" %d", &annettuKasvi.satoaika);

            if (annettuKasvi.satoaika < 1 ||annettuKasvi.satoaika > 12)
                printf("Virheellinen kuukausi! Yrit\x84 uudestaan:\n");
        }
        while (annettuKasvi.satoaika < 1 || annettuKasvi.satoaika > 12);
    }

    // Kysytään sitten käyttäjältä mihin kasvi sijoitetaan ja kuinka paljon sitä kylvetään.
    // Pyydetään ensin käyttäjältä kasvien lähtökulman paikka eli luoteiskulma.

    do
    {
        sopii = 1;

        printf("Mihin kasvi sijoitetaan?");
        printf("\nAnna et\x84isyys pohjoissreunasta: ");
        scanf(" %d", &alkurivi);
        printf("Anna et\x84isyys l\x84nsisreunasta: ");
        scanf(" %d", &alkusarake);


        // Seuraavaksi pyydetään kasvin kylvöpituus ja -leveys.
        printf("\nMiten monta metri\x84 kasvia kylvet\x84\x84n: ");
        scanf(" %d", &kasvipituus);
        printf("Kuinka leve\x84sti kasvia kylvet\x84\x84n: ");
        scanf(" %d", &kasvileveys);

        if (alkurivi + kasvipituus >= pituus)
        {
            sopii = 0;
            printf ("Virhe! Annettu kasvi ei sovi alueellle! Sy\x94t\x84 arvot uudestaan:\n");
            break;
        }
        if (alkusarake + kasvileveys >= leveys)
        {
            sopii = 0;
            printf ("Virhe! Annettu kasvi ei sovi alueellle! Sy\x94t\x84 arvot uudestaan:\n");
            break;
        }

    }
    while(sopii == 0);

    // Varmistetaan, että kasvi ei tule minkään jo olemassa olevan päälle.
    // Jos näin olisi käymässä, laitetaan kasvin ok-muuttuja nollaksi ja poistutaan funktiosta.
    for (i = alkurivi; i < alkurivi+kasvipituus; i++)
    {
        for (j = alkusarake; j < alkusarake+kasvileveys; j++)
        {
            if (alue[i][j] != 't')
            {
                printf("Virhe! Annetulla alueella on jo joko kasveja tai rakennuksia!");
                annettuKasvi.ok = 0;
                return annettuKasvi;
            }
        }
    }

    // Lisätään lopuksi kasvia esittävät merkit taulukkoon
    for (i = alkurivi; i < (alkurivi+kasvipituus); i++)
    {
        for (j = alkusarake ; j < (alkusarake+kasvileveys); j++)
        {
            alue[i][j] = annettuKasvi.merkki;
        }
    }

    annettuKasvi.ok = 1;
    return annettuKasvi;
}

/******************************************************************************************************/

void tulostaTiedot(struct kasvi kasvit[], int kasvienlkm)
{
    char kysytty;
    int i;

    // Pyydetään käyttäjältä halutun kasvin merkki
    printf("\nMink\x84 kasvin tiedot haluaisit n\x84hd\x84?\n");
    printf("Sy\x94t\x84 kasvin merkki: ");
    scanf(" %c", &kysytty);

    for (i = 0; i < kasvienlkm; i++)
    {
        if (kasvit[i].merkki == kysytty)
        {
            printf("\nKasvin nimi: \t\t %s\n", kasvit[i].nimi);
            printf("Kasvin merkki: \t\t %c\n", kasvit[i].merkki);
            printf("Kasvien kasteluv\x84li: \t %d p\x84iv\x84\x84\n", kasvit[i].kasteluvali);
            printf("Kasvin kylv\x94kuukausi: \t ");
            switch (kasvit[i].kylvoaika)
            {
                case(1) :   printf("tammikuu");
                            break;
                case(2) :   printf("helmikuu");
                            break;
                case(3) :   printf("maaliskuu");
                            break;
                case(4) :   printf("huhtikuu");
                            break;
                case(5) :   printf("toukokuu");
                            break;
                case(6) :   printf("kes\x84kuu");
                            break;
                case(7) :   printf("hein\x84kuu");
                            break;
                case(8) :   printf("elokuu");
                            break;
                case(9) :   printf("syyskuu");
                            break;
                case(10) :  printf("lokakuu");
                            break;
                case(11) :  printf("marraskuu");
                            break;
                case(12) :  printf("joulukuu");
                            break;
            }
            printf("\n");
            printf("Kasvin satokuukausi: \t ");
            switch (kasvit[i].satoaika)
            {
                case(1) :   printf("tammikuu");
                            break;
                case(2) :   printf("helmikuu");
                            break;
                case(3) :   printf("maaliskuu");
                            break;
                case(4) :   printf("huhtikuu");
                            break;
                case(5) :   printf("toukokuu");
                            break;
                case(6) :   printf("kes\x84kuu");
                            break;
                case(7) :   printf("hein\x84kuu");
                            break;
                case(8) :   printf("elokuu");
                            break;
                case(9) :   printf("syyskuu");
                            break;
                case(10) :  printf("lokakuu");
                            break;
                case(11) :  printf("marraskuu");
                            break;
                case(12) :  printf("joulukuu");
                            break;
            }
            printf("\n");
        }
    }
    printf("\n\nPaina mit\x84 tahansa nappia jatkaaksesi..");
    _getch();
}


/******************************************************************************************************/


void tulostaKaikki(struct kasvi kasvit[], int kasvienlkm)
{
    int i;

    printf("\n**********************************************************");
    for (i = 0; i < kasvienlkm; i++)
    {
        printf("\nKasvin nimi: \t\t %s\n", kasvit[i].nimi);
        printf("Kasvin merkki: \t\t %c\n", kasvit[i].merkki);
        printf("Kasvien kasteluv\x84li: \t %d p\x84iv\x84\x84\n", kasvit[i].kasteluvali);
        printf("Kasvin kylv\x94kuukausi: \t ");
        switch (kasvit[i].kylvoaika)
        {
            case(1) :   printf("tammikuu");
                        break;
            case(2) :   printf("helmikuu");
                        break;
            case(3) :   printf("maaliskuu");
                        break;
            case(4) :   printf("huhtikuu");
                        break;
            case(5) :   printf("toukokuu");
                        break;
            case(6) :   printf("kes\x84kuu");
                        break;
            case(7) :   printf("hein\x84kuu");
                        break;
            case(8) :   printf("elokuu");
                        break;
            case(9) :   printf("syyskuu");
                        break;
            case(10) :  printf("lokakuu");
                        break;
            case(11) :  printf("marraskuu");
                        break;
            case(12) :  printf("joulukuu");
                        break;
        }
        printf("\n");
        printf("Kasvin satokuukausi: \t ");
        switch (kasvit[i].satoaika)
        {
            case(1) :   printf("tammikuu");
                        break;
            case(2) :   printf("helmikuu");
                        break;
            case(3) :   printf("maaliskuu");
                        break;
            case(4) :   printf("huhtikuu");
                        break;
            case(5) :   printf("toukokuu");
                        break;
            case(6) :   printf("kes\x84kuu");
                        break;
            case(7) :   printf("hein\x84kuu");
                        break;
            case(8) :   printf("elokuu");
                        break;
            case(9) :   printf("syyskuu");
                        break;
            case(10) :  printf("lokakuu");
                        break;
            case(11) :  printf("marraskuu");
                        break;
            case(12) :  printf("joulukuu");
                        break;
        }
        printf("\n");
        printf("**********************************************************");
    }
    printf("\n\nPaina mit\x84 tahansa nappia jatkaaksesi..");
    _getch();
}


/******************************************************************************************************/


void poista(int pituus, int leveys, char alue[][leveys])
{
    int i, j;
    int sopii;
    int alkurivi, loppurivi, alkusarake, loppusarake;

    // Kysytään käyttäjältä mikä alue halutaan tyhjentää
    do
    {
        sopii = 1;
        printf("Anna rivi ensimm\x84inen rivi, jonka haluat tyhjent\x84\x84: ");
        scanf(" %d", &alkurivi);
        printf("Anna viimeinen rivi, jonka haluat tyhjent\x84\x84: ");
        scanf(" %d", &loppurivi);
        printf("Anna ensimm\x84inen sarake, jonka haluat tyhjent\x84\x84: ");
        scanf(" %d", &alkusarake);
        printf("Anna viimeinen sarake, jonka haluat tyhjent\x84\x84: ");
        scanf(" %d", &loppusarake);

        if (alkurivi < 1 || alkusarake < 1 || loppurivi >= pituus ||loppusarake >= leveys)
        {
            sopii = 0;
            printf("Virhe! Yrit\x8t tyhjent\x84\x84 alueen reunojen ulkopuolelta.\n");
            printf("Yrit\x84 uudestaan.\n");
            break;
        }
        else
        {
            for (i = alkurivi; i <= loppurivi; i++)
            {
                for (j = alkusarake; j <= loppusarake; j++)
                {
                    // Vaihdetaan kaikki kyseisen alueen merkit takaisin t-merkeiksi (HUOM! Saatetaan turhaan vaihtaa t-merkkejä t-merkeiksi)
                    alue[i][j] = 't';
                }
            }
        }

    }
    while (sopii == 0);
}


/******************************************************************************************************/


void tallennaTiedostoon(int pituus, int leveys, char alue[][leveys], struct kasvi kasvit[], int kasvienlkm)
{
    int i, j;

    // Avataan tiedosto kirjoittamista varten
    fptr = fopen("D:\\C Ohjelmointi\\C Programming Absolute Beginners Guide\\Harjoitustyät\\Maansuunnittelu\\tallennus.txt", "w");

    // Varmistetaan, että tiedosto avattiin onnistuneesti
    if (fptr == 0)
    {
        printf("Virhe! Tiedostoa ei onnistuttu avamaan tallentamista varten.\n");
        exit (1);
    }

    fprintf(fptr, "Tässä maansunnittelussa luotu karttasi ja kasviluettelo:\n\n");
    for (i = 0; i < pituus; i++)
    {
        for (j = 0; j < leveys; j++)
        {
            // Jos taulukon arvo on t (=tyhjä) tai r (=rakennus), tulostetaan välilyänti
            if (alue[i][j] == 't' || alue[i][j] == 'r')
                fprintf(fptr, " ");

            // Muussa tapauksessa tulostetaan taulukon merkki.
            else
                fprintf(fptr, "%c", alue[i][j]);
        }
        fprintf(fptr, "\n");
    }


    fprintf(fptr, "\nKasviluettelo: \n");
    fprintf(fptr, "\n**********************************************************");
    for (i = 0; i < kasvienlkm; i++)
    {
        fprintf(fptr, "\nKasvin nimi: \t\t %s\n", kasvit[i].nimi);
        fprintf(fptr, "Kasvin merkki: \t\t %c\n", kasvit[i].merkki);
        fprintf(fptr, "Kasvien kasteluväli: \t %d päivää\n", kasvit[i].kasteluvali);
        fprintf(fptr, "Kasvin kylväkuukausi: \t ");
        switch (kasvit[i].kylvoaika)
        {
            case(1) :   fprintf(fptr, "tammikuu");
                        break;
            case(2) :   fprintf(fptr, "helmikuu");
                        break;
            case(3) :   fprintf(fptr, "maaliskuu");
                        break;
            case(4) :   fprintf(fptr, "huhtikuu");
                        break;
            case(5) :   fprintf(fptr, "toukokuu");
                        break;
            case(6) :   fprintf(fptr, "kesäkuu");
                        break;
            case(7) :   fprintf(fptr, "heinäkuu");
                        break;
            case(8) :   fprintf(fptr, "elokuu");
                        break;
            case(9) :   fprintf(fptr, "syyskuu");
                        break;
            case(10) :  fprintf(fptr, "lokakuu");
                        break;
            case(11) :  fprintf(fptr, "marraskuu");
                        break;
            case(12) :  fprintf(fptr, "joulukuu");
                        break;
        }
        fprintf(fptr, "\n");
        fprintf(fptr, "Kasvin satokuukausi: \t ");
        switch (kasvit[i].satoaika)
        {
            case(1) :   fprintf(fptr, "tammikuu");
                        break;
            case(2) :   fprintf(fptr, "helmikuu");
                        break;
            case(3) :   fprintf(fptr, "maaliskuu");
                        break;
            case(4) :   fprintf(fptr, "huhtikuu");
                        break;
            case(5) :   fprintf(fptr, "toukokuu");
                        break;
            case(6) :   fprintf(fptr, "kesäkuu");
                        break;
            case(7) :   fprintf(fptr, "heinäkuu");
                        break;
            case(8) :   fprintf(fptr, "elokuu");
                        break;
            case(9) :   fprintf(fptr, "syyskuu");
                        break;
            case(10) :  fprintf(fptr, "lokakuu");
                        break;
            case(11) :  fprintf(fptr, "marraskuu");
                        break;
            case(12) :  fprintf(fptr, "joulukuu");
                        break;
        }
        fprintf(fptr, "\n");
        fprintf(fptr, "**********************************************************");
    }

    fclose(fptr);
}
